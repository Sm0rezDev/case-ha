resource "linode_instance" "web" {
    //for_each = var.linode_instance
    count = 2
    label = "web-${count.index}"
    image = "linode/ubuntu18.04"
    region = "se-sto"
    type = "g6-nanode-1"
    authorized_keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDyLtdzBpyWBer1HLjpTqEnTLzFGDKEJNRfQxx0szDE+ sm0rezdev@R932070S",
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOaYr1kWILkZGL0CmWPuvJwyRhYaJRu1xfxqRRtPCHN7"
    ]
    root_pass = "terr4form-test"
}

# resource "linode_instance_ip" "web_ip" {
#     linode_id = linode_instance.web[*].ip_address
#     public = true
# }

output "ip" {
  value = linode_instance.web[*].ip_address
}