terraform {
  required_providers {
    linode = {
      source = "linode/linode"
      version = "2.9.2"
    }
  }
  backend "http" {}
}

provider "linode" {
  # Configuration options
}

